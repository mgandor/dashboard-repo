FROM node:8
WORKDIR /home/node/app
COPY . .
RUN npm install --no-cache
CMD ["npm", "start"]