# GitLab CI in conjunction with GitLab Runner can use Docker Engine to test and build any application.
# Docker, when used with GitLab CI, runs each job in a separate and isolated container using the predefined image that is set up in .gitlab-ci.yml.
# In this case we use the latest node docker image to build and test this project.
image: node:alpine

# Docker in Docker (dind) allows this CI file to be run locally (assuming you have docker installed).
# Note this relies on a number of system variables on the local system. 
# TODO: Find an elegant way of setting these in a robust way without exposing them in a public file.
#
#    DOCKER_REGISTRY_USER_LOGIN, DOCKER_REGISTRY_USER, DOCKER_REGISTRY_HOST,  DOCKER_AUTH_CONFIG, 
#    CI_REGISTRY_PASS_LOGIN, CI_REGISTRY 

services:
  - docker:dind

# cache is used to specify a list of files and directories which should be cached between jobs. You can only use paths that are within the project workspace.
# If cache is defined outside the scope of jobs, it means it is set globally and all jobs will use that definition
cache:
  paths:
    - build

# before_script is used to define the command that should be run before all jobs, including deploy jobs, but after the restoration of artifacts.
# This can be an array or a multi-line string. In this case node install will install all our dependencies including the scripts
# before_script:
#   - npm ci

# stages is used to define the workflow. So in this example we have a 'test' stage followed by a 'deploy' stage.  What this means is first any jobs with a stage of test are
# run. Jobs of the same stage  may be run in parallel. If they succeed and then any jobs with a stage of 'deploy' are run.
#
# If we hadn't defined any stages then we could still have used the defaults of 'build' 'test' and 'deploy' for defining the jobs.
#
# (if you don't specify a stage in the job then it is assumed to be part of the 'test' stage)
stages:
  - dependencies
  - test
  - deploy
  - image

# The YAML file defines a set of jobs with constraints stating when they should be run.
# You can specify an unlimited number of jobs which are defined as top-level elements with an arbitrary name and always have to contain at least the script clause.
# In this case we have only the test job which produce an artifacts (it must be placed into a directory called "public")
# It is also specified that only the master branch will be subject of this job.

install_dependencies:
  stage: dependencies
  script:
    - npm ci
  cache:
    paths:
      - node_modules/
    policy: push

# run the test linting and coverage reports
test_coverage:
  stage: test
  cache:
    paths:
      - node_modules/
    policy: pull
  script:
    - mkdir -p build/reports/
    - npm run test
    - mv junit.xml build/reports/unit-tests.xml
    - npm run linting
    - mv linting.xml build/reports/linting.xml
    - npm run coverage
    - mv build/coverage/cobertura-coverage.xml build/reports/code-coverage.xml
  artifacts:
    paths:
      - build/

# list the current top level dependencies for the project
# TODO We could write a script to convert the json and
#      write it into something
list_dependencies:
  stage: test
  allow_failure: true
  cache:
    paths:
      - node_modules/
    policy: pull
  script:
    - npm list --depth=0 --json >> npm_deps.json
    - npm list --depth=0  >> npm_deps.txt
    - mkdir .public
    - cp npm_deps.txt .public/
    - cp npm_deps.json .public/
  artifacts:
    paths:
      - .public

# Run the gitlab (code climate)  code quality checks
# TODO: the codeclinmmate test reporter is depricated so need to fix
code_quality:
  stage: test
  image: docker:stable
  variables:
    DOCKER_DRIVER: overlay2
  allow_failure: true
  services:
    - docker:stable-dind
  cache:
    paths:
      - node_modules/
    policy: pull
  script:
    - export SP_VERSION=$(echo "$CI_SERVER_VERSION" | sed 's/^\([0-9]*\)\.\([0-9]*\).*/\1-\2-stable/')
    - docker run
      --env SOURCE_CODE="$PWD"
      --volume "$PWD":/code
      --volume /var/run/docker.sock:/var/run/docker.sock
      "registry.gitlab.com/gitlab-org/security-products/codequality:$SP_VERSION" /code
    - more gl-code-quality-report.json
  artifacts:
    paths: [gl-code-quality-report.json]

# Build the application and copy reports
build_deploy:
  stage: deploy
  dependencies:
    - test_coverage
    - code_quality
  cache:
    paths:
      - node_modules/
    policy: pull
  script:
    - npm install 

  artifacts:
    paths:
      - public
    expire_in: 30 days

build_image:
  stage: image
  image: docker:stable
  before_script:
  - docker login -u $DOCKER_REGISTRY_USERNAME -p $DOCKER_REGISTRY_PASSWORD $DOCKER_REGISTRY_HOST
  script:
    - docker build . -t nexus.engageska-portugal.pt/ska-docker/webjive-develop_dashboards:master --label GIT_COMMIT=$CI_COMMIT_SHA
    - docker push nexus.engageska-portugal.pt/ska-docker/webjive-develop_dashboards:master
  only:
    - master

  stage: image
  image: docker:stable
  before_script:
  - docker login -u $DOCKER_REGISTRY_USERNAME -p $DOCKER_REGISTRY_PASSWORD $DOCKER_REGISTRY_HOST
  script:
    - docker build . -t nexus.engageska-portugal.pt/ska-docker/webjive-develop_dashboards:develop --label GIT_COMMIT=$CI_COMMIT_SHA
    - docker push nexus.engageska-portugal.pt/ska-docker/webjive-develop_dashboards:develop
  only:
    - develop

create_ci_metrics:
  stage: .post
  image: nexus.engageska-portugal.pt/ska-docker/ska-python-buildenv:latest
  when: always
  tags:
    - docker-executor
  script:
    # Gitlab CI badges creation: START
    - apt-get -y update
    - apt-get install -y curl --no-install-recommends
    - curl -s https://gitlab.com/ska-telescope/ci-metrics-utilities/raw/master/scripts/ci-badges-func.sh | sh
    # Gitlab CI badges creation: END
  artifacts:
    paths:
      - ./build