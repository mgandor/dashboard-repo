"use strict";
/**
 * Handles urls prefixed with /dashboards
 */
const express = require("express");
const jsonwebtoken = require("jsonwebtoken");
const mongoose = require("mongoose");
const router = express.Router();
var Dashboard = require("../models/dashboard");

/**
 * Returns a count of shared dashboards for each group the current user belongs to, for a specific tangodb.
 */
router.get("/group/dashboardsCount", async (req, res, next) => {
  const token = req.cookies.webjive_jwt;
  const excludeCurrentUser = req.query.excludeCurrentUser === "true";
  const tangoDB = req.query.tangoDB || "";
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  const data = {};
  for (let i = 0; i < groups.length; i++){
    try{
      //allowing missing tango db is really just for backwards compatibility. Can be removed when all dashboard has this set
      const query = {group: groups[i], deleted: { $ne: true }, tangoDB: {$in: ["", tangoDB, null]} }
      if (excludeCurrentUser){
        query["user"] = {$ne: username}
      }
        data[groups[i]] = await Dashboard.countDocuments(query)
    }catch(error){
        console.log(error);
    }
  }
  res.send(data);
});
/**
 * Return a list of dashboard that belongs to a specific group for a specific tangodb.
 * Does not return the widgets
 * returns
 * id:string
 * name: string
 * user: string
 * insertTime: Date
 * updateTime: Date
 * group: AD group this dashboard is shared with, or null
 */
router.get("/group/dashboards", function(req, res, next) {
  const token = req.cookies.webjive_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  const requestedGroup = req.query.group;
  const tangoDB = req.query.tangoDB || "";
  const excludeCurrentUser = req.query.excludeCurrentUser  === "true";
  if (!requestedGroup){
      res.status(400).send("Missing query parameter group");
      return;
  }
  if (!groups.includes(requestedGroup)) {
    res.sendStatus(403);
    return;
  }
  const query = { group: requestedGroup, deleted: { $ne: true }, tangoDB: {$in: ["", tangoDB, null]} }
  if (excludeCurrentUser){
    query["user"] = {$ne: username}
  }
  Dashboard.find(query, function(
    err,
    result
  ) {
    if (err) {
      res.sendStatus(404);
    } else {
      if (result) {
        var list = [];
        result.forEach(function(result) {
          const {
            name,
            user,
            insertTime,
            updateTime,
            group,
            lastUpdatedBy,
            tangoDB,
          } = result;
          list.push({
            id: result._id,
            name,
            user,
            insertTime,
            updateTime,
            group,
            lastUpdatedBy,
            tangoDB
          });
        });
        res.send(list);
      }
    }
  });
});

/**
 * Return a list of dashboard that belongs to a specific user for a specific tangodb.
 * Does not return the widgets
 * returns
 * id:string
 * name: string
 * user: string
 * insertTime: Date
 * updateTime: Date
 * group: AD group this dashboard is shared with, or null
 */
router.get("/user/dashboards", function(req, res, next) {
  const token = req.cookies.webjive_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  const tangoDB = req.query.tangoDB || "";
  Dashboard.find({ user: username, deleted: { $ne: true }, tangoDB: {$in: ["", tangoDB, null]} }, function(
    err,
    result
  ) {
    if (err) {
      res.sendStatus(404);
    } else {
      if (result) {
        var list = [];
        result.forEach(function(result) {
          const {
            name,
            user,
            insertTime,
            updateTime,
            group,
            lastUpdatedBy,
            tangoDB
          } = result;
          list.push({
            id: result._id,
            name,
            user,
            insertTime,
            updateTime,
            group,
            lastUpdatedBy,
            tangoDB
          });
        });
        res.send(list);
      }
    }
  });
});

/**
 * Return a full dashboard by id.
 * returns
 * id:string
 * name: string
 * user: string
 * insertTime: Date
 * updateTime: Date
 * widgets: any[]
 */
router.get("/:id", function(req, res, next) {
  var id = req.params.id;
  Dashboard.findOne({ _id: id, deleted: { $ne: true } }, function(err, result) {
    if (err) {
      res.sendStatus(404);
    } else {
      if (result) {
        console.log("Serving dashboard: " + result._id);
        const {
          name,
          user,
          insertTime,
          updateTime,
          group,
          lastUpdatedBy,
          tangoDB
        } = result;
        res.send({
          id: result._id,
          name,
          user,
          insertTime,
          updateTime,
          widgets: result.widgets,
          group,
          lastUpdatedBy,
          tangoDB
        });
      } else {
        res.sendStatus(404);
      }
    }
  });
});
/**
 * Delete a dashboard that belongs to the user by id
 * returns
 * id: string
 * deleted: boolean
 */
router.delete("/:id", function(req, res, next) {
  var id = req.params.id;
  var token = req.cookies.webjive_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  Dashboard.findOneAndUpdate(
    { _id: id, user: username, deleted: { $ne: true } },
    { $set: { deleted: true } },
    { new: true },
    function(err, result) {
      if (err) {
        res.send({ id, deleted: false });
      } else {
        if (result) {
          res.send({ id: result._id, deleted: true });
        } else {
          res.sendStatus(403);
        }
      }
    }
  );
});
/**
 * Share a dashboard to an AD group
 */
router.post("/:id/share", function(req, res) {
  var id = req.params.id;
  var token = req.cookies.webjive_jwt;
  var group = req.body.group;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  console.log("Sharing dashboard " + id);
  Dashboard.findById(id, function(err, dashboard) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      return;
    }
    if (!dashboard) {
      res.sendStatus(404);
      return;
    }
    if (dashboard.user !== username) {
      res.sendStatus(403);
      return;
    }
    dashboard.group = group;
    dashboard.save(function(err) {
      if (err) {
        res.sendStatus(500);
        console.log(err);
      } else {
        res.send({ id: dashboard._id + "", created: false });
      }
    });
  });
});
/**
 * Clone a dashboard using id
 */
router.post("/:id/clone", function(req, res, next) {
  var id = req.params.id;
  var token = req.cookies.webjive_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  Dashboard.findOne({ _id: id, deleted: { $ne: true } }, function(err, result) {
    if (err) {
      console.log(err);
      res.sendStatus(404);
    } else {
      if (result) {
        insertDashboard(result.name, username, result.widgets, result.tangoDB, res);
      } else {
        res.sendStatus(404);
      }
    }
  });
});
/**
 * Rename a dashboard using id
 * returns id
 */
router.post("/:id/rename", function(req, res, next) {
  var id = req.params.id;
  var new_name = req.body.newName;
  var token = req.cookies.webjive_jwt;
  let decoded = {};
  try {
    decoded = decodeToken(token);
  } catch (error) {
    res.sendStatus(401);
    return;
  }
  const { username, groups } = decoded;
  Dashboard.findOneAndUpdate(
    {
      _id: new mongoose.mongo.ObjectID(id),
      user: username,
      deleted: { $ne: true }
    },
    { $set: { name: new_name } },
    { new: true },
    function(err, result) {
      if (err) {
        res.send(404);
      } else {
        if (result) {
          res.send({ id: result._id });
        } else {
          res.sendStatus(404);
        }
      }
    }
  );
});
/**
 * Creates or updates a dashboard.
 * returns
 * id:string - the id of the dashboard that was updated or created
 * created:boolean - true if the dashboard was created
 */
router.post("/", function(req, res, next) {
  var body = req.body;
  var id = body.id;
  var name = body.name;
  var widgets = body.widgets;
  var tangoDB = body.tangoDB || "";
  var token = req.cookies.webjive_jwt;
  const { username, groups } = decodeToken(token);

  if (id && id !== "") {
    try {
      updateDashboard(id, username, groups, widgets, tangoDB, res);
    } catch (err) {
      console.log(err);
      res.sendStatus(404);
    }
  } else {
    try {
      insertDashboard(name, username, widgets, tangoDB, res);
    } catch (err) {
      console.log(err);
      res.sendStatus(404);
    }
  }
});

const updateDashboard = (id, username, groups, widgets, tangoDB, res) => {
  console.log("Updating dashboard " + id);
  Dashboard.findById(id, function(err, dashboard) {
    if (err) {
      console.log(err);
      res.sendStatus(500);
      return;
    }
    if (!dashboard) {
      res.sendStatus(404);
      return;
    }
    const mine = dashboard.user === username;
    const sharedWithMe = dashboard.group && groups.includes(dashboard.group);
    if (!(mine || sharedWithMe)) {
      res.sendStatus(403);
      return;
    }
    dashboard.widgets = widgets;
    dashboard.updateTime = new Date();
    dashboard.lastUpdatedBy = username;
    dashboard.tangoDB = tangoDB;
    dashboard.markModified("widgets");
    dashboard.save(function(err) {
      if (err) {
        res.sendStatus(500);
        console.log(err);
      } else {
        res.send({ id: dashboard._id + "", created: false });
      }
    });
  });
};
const insertDashboard = (name, username, widgets, tangoDB, res) => {
  var dashboard = new Dashboard();
  console.log("Inserting dashboard " + dashboard._id);
  dashboard.widgets = widgets;
  dashboard.user = username;
  dashboard.name = name || "Untitled dashboard";
  dashboard.insertTime = new Date();
  dashboard.lastUpdatedBy = username;
  dashboard.tangoDB = tangoDB;
  dashboard.save(function(err) {
    if (err) {
      res.sendStatus(500);
      console.log("ERROR?");
      console.dir(err);
    } else {
      res.send({ id: dashboard._id + "", created: true });
    }
  });
};

function decodeToken(token) {
  try {
    const decoded = jsonwebtoken.verify(token, process.env.SECRET);
    return decoded;
  } catch (exception) {
    throw Error("jwt authorization failed");
  }
}

module.exports = router;
